import React, { useState, useEffect } from 'react';
import { confirmPayment } from '../services/api';
import { useParams, useNavigate } from 'react-router-dom';
import Authentification from './Authentification';

const Paiement = () => {
    const history = useNavigate();
    const [confirmation, setConfirmation] = useState(null);
    const [showConfirmation, setShowConfirmation] = useState(false); // Nouvel état pour gérer l'affichage

    const param = useParams();
    const orderId = param.id;

    const [isAuthenticated, setIsAuthenticated] = useState(false);

    const handleAuth = () => {
        setIsAuthenticated(true);
    };

    useEffect(() => {
        // Vérifiez si un token est présent dans le localStorage
        const storedToken = localStorage.getItem('token');
        if (storedToken) {
            // Mettez à jour l'état d'authentification
            setIsAuthenticated(true);
        }
    }, []); // Le tableau vide [] signifie que cela ne s'exécutera qu'une fois lors du montage initial

    const handleConfirmPayment = async () => {
        try {
            const response = await confirmPayment(orderId);
            setConfirmation(response);
            // Affichez la fenêtre de confirmation en modifiant l'état
            setShowConfirmation(true);
        } catch (error) {
            console.error(error);
        }
    };

    const handleOkClick = () => {
        // Redirigez l'utilisateur après avoir cliqué sur le bouton "OK"
        history(`/`);
        // Cachez la fenêtre de confirmation en modifiant l'état
        setShowConfirmation(false);
    };

    if (!isAuthenticated) {
        return <Authentification onAuth={handleAuth} />;
    } else {
        return (
            <div className={'body'}>
                <h2>Application Mcommerce</h2>
                <h3>Paiement</h3>
                <button onClick={handleConfirmPayment} className={'image'}>
                    Confirmer le paiement
                </button>
                {showConfirmation && (
                    <div className="confirmation-modal">
                        <p>{confirmation}</p>
                        <button onClick={handleOkClick}>OK</button>
                    </div>
                )}
            </div>
        );
    }
};

export default Paiement;
