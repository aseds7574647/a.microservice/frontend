// src/components/Inscription.js
import React, { useState } from 'react';
import { auth } from '../services/api';
import {json} from "react-router-dom";

const Inscription = ({ onSign }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [emailError, setEmailError] = useState('');
    const [passwordError, setPasswordError] = useState('');

    const handleInscription = async () => {
        // Réinitialisez les erreurs
        setEmailError('');
        setPasswordError('');

        try {
            const response = await auth.register(email, password);

            // Vérifiez si la réponse contient un token avant de le déstructurer
            if (response && response.token) {
                const token = response.token;
                // Stockez le token dans localStorage ou un autre endroit sécurisé
                localStorage.setItem('token', token);
                // Redirigez l'utilisateur vers la page d'accueil après l'inscription réussie
                onSign();
            } else {
                console.error('La réponse ne contient pas de token:', response);
            }
        } catch (error) {
            // Gérez les erreurs spécifiques liées à l'inscription
            if (error || String(error).includes('email') || String(error).includes('mot de passe')) {
                // Erreur de format d'email
                if (String(error).includes('email')) {
                    setEmailError('Format d\'email invalide');
                }
                // Erreur de mot de passe court
                if (String(error).includes('mot de passe')) {
                    setPasswordError('Le mot de passe doit avoir au moins 5 caractères');
                }
            }
            else {
                console.error('Erreur lors de l\'inscription:', error);
            }
        }
    };

    return (
        <div>
            <h2>Inscription</h2>
            {emailError && <p className="error">{emailError}</p>}
            {passwordError && <p className="error">{passwordError}</p>}
            <input type="text" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
            <input type="password" placeholder="Mot de passe" value={password} onChange={(e) => setPassword(e.target.value)} />
            <button type={"submit"} onClick={handleInscription}>S'inscrire</button>
        </div>
    );
};

export default Inscription;
