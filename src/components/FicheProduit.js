// src/components/FicheProduit.js
import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { createOrder, getProduct } from '../services/api';
import Modal from 'react-modal';
import AuthModal from './Authentification';
import Inscription from './Inscription';

const FicheProduit = () => {
    const history = useNavigate();
    const param = useParams();
    const [product, setProduct] = useState(null);
    const [isAuthModalOpen, setIsAuthModalOpen] = useState(false);
    const [isInscriptionModalOpen, setIsInscriptionModalOpen] = useState(false);
    const productId = param.id;

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await getProduct(`${productId}`);
                setProduct(data);
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, [productId]);

    if (!product) {
        return <div>Chargement des détails du produit...</div>;
    }

    const handleCreateOrder = () => {
        // Vérifiez si l'utilisateur est authentifié
        const isAuthenticated = !!localStorage.getItem('token');

        if (isAuthenticated) {
            // Créez la commande si l'utilisateur est authentifié
            createOrder(product.title, (error, newOrder) => {
                if (error) {
                    console.error('Erreur lors de la création de la commande :', error);
                } else {
                    const newCommandeId = newOrder._id;
                    history(`/paiement/${newCommandeId}`);
                }
            });
        } else {
            // Ouvrez le modal d'authentification si l'utilisateur n'est pas authentifié
            setIsAuthModalOpen(true);
        }
    };

    const handleAuthModalClose = () => {
        // Cette fonction sera appelée lorsque l'authentification est réussie ou lorsque le modal est fermé
        setIsAuthModalOpen(false);
    };

    const handleInscriptionModalClose = () => {
        // Cette fonction sera appelée lorsque l'inscription est réussie ou lorsque le modal est fermé
        setIsInscriptionModalOpen(false);
    };

    const openInscriptionModal = () => {
        // Ouvrez le modal d'inscription
        setIsInscriptionModalOpen(true);
    };

    return (
        <div className={'body'}>
            <h2>Application Mcommerce</h2>
            <div>
                <img src={product.imageUrl} alt={'C\'est un produit'} />
            </div>
            <h3>{product.title}</h3>
            <p>{product.description}</p>
            <button onClick={handleCreateOrder} className={'image'}>
                COMMANDER
            </button>
            {/* Modal d'authentification */}
            <Modal
                className={'auth'}
                isOpen={isAuthModalOpen}
                onRequestClose={handleAuthModalClose}
                ariaHideApp={false} // Option pour éviter un avertissement lié à l'accessibilité
            >
                <AuthModal
                    isOpen={isAuthModalOpen}
                    onRequestClose={handleAuthModalClose}
                    onAuth={(message) => {
                        <p>${message}</p>
                        // Vous pouvez effectuer des actions supplémentaires après l'authentification
                    }}
                />
                <p>
                    Vous n'avez pas de compte ?{' '}
                    <button onClick={openInscriptionModal}>Inscrivez-vous ici</button>
                </p>
            </Modal>
            {/* Modal d'inscription */}
            <Modal
                className={'auth'}
                isOpen={isInscriptionModalOpen}
                onRequestClose={handleInscriptionModalClose}
                ariaHideApp={false}
            >
                <Inscription onSign={handleInscriptionModalClose} />
            </Modal>
        </div>
    );
};

export default FicheProduit;