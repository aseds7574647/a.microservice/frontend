// Authentification.js
import React, { useState } from 'react';
import { auth } from '../services/api';

const AuthModal = ({ isOpen, onRequestClose, onAuth }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleLogin = async () => {
        try {
            const response = await auth.login(email, password);

            // Vérifiez si la réponse contient un token avant de le déstructurer
            if (response && response.token) {
                const token = response.token;
                // Stockez le token dans localStorage ou un autre endroit sécurisé
                localStorage.setItem('token', token);

                // Informez le composant parent que l'authentification a réussi
                onAuth('Authentification réussie');
                // Fermez le modal
                onRequestClose();
            } else {
                console.error('La réponse ne contient pas de token:', response);
            }
        } catch (error) {
            console.error('Erreur d\'authentification', error);
            // Affichez le message d'erreur
            setError('Email ou mot de passe invalide');
        }
    };

    return (
        <div>
            <h2>Authentification</h2>
            {error && <p className="error">{error}</p>}
            <input type="text" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
            <input type="password" placeholder="Mot de passe" value={password} onChange={(e) => setPassword(e.target.value)} />
            <button type={"submit"} onClick={handleLogin}>Se connecter</button>
        </div>
    );
};

export default AuthModal;
